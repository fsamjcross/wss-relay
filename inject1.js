	var imported = document.createElement('script');
	imported.src = 'https://cdn.socket.io/socket.io-1.4.5.js';
	document.head.appendChild(imported);

imported.onload = function() {
  var socket = io.connect('https://cross-home.dyndns.org:8888', {secure: true});
  var vid = document.querySelector('video')

	vid.addEventListener("play", function() {
			socket.emit('state','play');
	});

	vid.addEventListener("pause", function() {
		socket.emit('state','pause');
	});
	var seekedlock = false
	vid.addEventListener("seeked", function(e) {
		if (seekedlock == false) {
		socket.emit('seek',vid.currentTime);
		seekedlock = true
		setTimeout(function() {
			seekedlock = !false
		}, 100);
		}
	});

  socket.on('state', function (data) {
		if (data == 'play') {
			vid.play()
		}else{
			vid.pause()
		}
	});
	socket.on('seek', function (data) {
		vid.currentTime = data
	});	
}