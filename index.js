var fs = require( 'fs' );
var app = require('express')();
var https        = require('https');
var server = https.createServer({
    key: fs.readFileSync('./key.pem'),
    cert: fs.readFileSync('./cert.pem'),
    ca: fs.readFileSync('./cert.pem'),
    requestCert: false,
    rejectUnauthorized: false
},app);
server.listen(8888);
var inject = fs.readFileSync('inject1-min.js', "utf8");
var io = require('socket.io').listen(server);

io.sockets.on('connection',function (socket) {
    console.log(':)')
    socket.on('state', function (data) {
    	console.log(data)
	    socket.broadcast.emit('state',data);
	});
	socket.on('seek', function (data) {
    	console.log(data)
	    socket.broadcast.emit('seek',data);
	});
});


app.get("/", function(request, response){
      response.send(inject);
})